-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 10 Sty 2022, 08:25
-- Wersja serwera: 10.4.21-MariaDB
-- Wersja PHP: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `3ti`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `tak`
--

CREATE TABLE `tak` (
  `id` int(11) NOT NULL,
  `pytanie` text NOT NULL,
  `odp a` varchar(250) NOT NULL,
  `odp b` varchar(250) NOT NULL,
  `odp c` varchar(250) NOT NULL,
  `odp d` varchar(250) NOT NULL,
  `prawidlowa odpowiedz` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Zrzut danych tabeli `tak`
--

INSERT INTO `tak` (`id`, `pytanie`, `odp a`, `odp b`, `odp c`, `odp d`, `prawidlowa odpowiedz`) VALUES
(1, 'Stosunek ładunku zgromadzonego na przewodniku do potencjału tego przewodnika określa jego', 'moc', 'rezystancję', 'indukcyjność', 'pojemność elektryczną', 'd'),
(2, 'Czynnym elementem elektronicznym jest', 'cewka', 'rezystor', 'tranzystor', 'kondensator', 'tranzystor'),
(0, 'Wskaż element, który dopasowuje poziom napięcia z sieci energetycznej przy użyciu transformatora \r\nprzenoszącego energię z jednego obwodu elektrycznego do drugiego z wykorzystaniem zjawiska indukcji \r\nmagnetycznej.\r\n', '\r\nRejestr szeregowy', 'Rezonator kwarcowy', 'Przerzutnik synchroniczny', 'Zasilacz transformatorowy', 'Zasilacz transformatorowy'),
(4, 'Przedstawiona karta przechwytująca wideo będzie współpracowała z płytą główną wyposażoną w port', 'AGP', 'PCI-e\r\n', '1-Wire', 'eSATA', 'PCI-e\r\n'),
(5, 'Podczas instalacji systemu Windows, tuż po uruchomieniu instalatora w trybie graficznym, możliwe jest \r\nuruchomienie Wiersza poleceń (konsoli) za pomocą kombinacji przycisków', 'ALT + F4', 'CTRL + Z', 'CTRL + Z', 'CTRL + SHIFT', 'CTRL + Z'),
(6, 'Po zainstalowaniu systemu Windows 10, aby skonfigurować połączenie internetowe z limitem danych, \r\nw ustawieniach sieci i Internetu należy ustawić połączenie', 'taryfowe', 'przewodowe', 'bezprzewodowe', 'szerokopasmowe', 'taryfowe');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
